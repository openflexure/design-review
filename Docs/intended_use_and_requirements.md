# Draft Intended Use and Requirements

This document is a working draft

Note that some previous notes on design inputs can be found in the design review meeting from [May 20201](../Minutes/2021_05_19.md).

# Intended Use Statement

*Indended use is for a specific output. The OpenFlexure Project itself should concentrate on the requirements that include more general design considerations to allow multiple manufacturers to take the design to their local market. The intended use statement below applies to the design and documentation, and would be refined by individual manufacturers to apply to their product(s).*

> The OpenFlexure Microscope is a motorised, digital, high-magnification optical microscope.  It is a general purpose laboratory instrument for diagnostic use by a trained user. Applications include (but are not limited to) parisitology, haematology, and histopathology. The microscope is intended to be used in fixed or mobile laboratories in urban and rural areas in any country. The microscope produces digital images, enabling record keeping or analysis by an expert at a remote location when used together with appropriate software. The microscope can automatically acquire mosaics or focal stacks to represent larger areas of the sample that are visible in a single image.

> The microscope is intended to be manufactured and maintained by local engineers using locally available resources. This is not part of the intended use of an end product, but is an important intended use of the designs that form the output of the OpenFlexure Project.

# Requirements
## Optical
1. The microscope shall image samples optically in transmission bright-field mode
1. The microscope shall produce digital images
1. The microscope shall use RMS threaded 40x (air immersion) or 100x (oil immersion) objectives
1. The microscope shall have sufficient optical performance to image common parasites (including *Plasmodium* and Helminth eggs) and stained tissue sections

## Mechanical
1. The microscope shall be motorised
1. The microscope shall move with sufficient precision in the focal direction to focus on a thin sample
1. The microscope shall move with sufficient precision laterally to move by much less than one field of view

## Software and usability
1. The microscope shall be able to autoamtically capture multiple image and to move any axis in between images
1. The microscope shall be usable by a laboratory technician with suitable training
1. The software shall provide a mechanism to store captured images on removable media
1. The software shall provide a mechanism to integrate with external image management or transmission software systems

## Maintenance
1. The microscope shall require servicing not more than once every 6 months (excluding routine maintenance by the user)
1. The microscope should be maintainable by a technician familiar with manual optical microscopes
1. Spare parts shall be locally manufacturable wherever possible
1. Requirements for specialised or proprietary tooling during maintanance shall be minimised

## Environment
1. The microscope shall be robust against dust ingress
1. The microscope shall be robust against shock during transport (e.g. in an off road vehicle)
1. The microscope shall function correctly when powered using batteries or mains power, and should provide appropriate options for power input
1. The micoscope shall function correctly in temperatures that range from 15C and 45C, and humidities between 0 and 100%
1. The microscope shall not require proprietary consumables
1. The microscope shall function correctly with respect to vibrations that may be reasonably expected on a laboratory bench

## Manufacture
1. The microscope shall minimise the use of specialist or hard to source components
1. The design shall minimise the requirement for expensive or specialist tools and facilities
1. The design shall be documented to a standard that enables consistent reproduction without out-of-band communication with the designers

