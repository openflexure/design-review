 
## 13 April 2022

**Meeting starts: 09:29 BST**

Attendees
* Richard Bowman, University of Bath (RWB)
* Julian Stirling, University of Bath (JS)
* Paul Nyakyi, Bongo Tech & Research Labs (PN)

## Agenda
1. New SD card image
2. Server configuration and management


## 1. New SD card image

RWB has been generating an updated SD card with the newest software on it.  PN expressed an interest in using this next week, on the order of 6 microscopes for IHI.  The image is built, but not yet tested: RWB to test it in a working microscope.  The new image updates the system packages and the microscope server, and also adds a Python environment that will make it much easier to run the commissioning scripts, for example.

## 2. Server configuration and management

As part of the ongoing effort to overhaul how the microscope is configured ([!147](https://gitlab.com/openflexure/openflexure-microscope-server/-/merge_requests/147)), RWB has implemented a very basic HTTP server that displays an error page detailing what part of the configuration failed to load.  Currently this process runs once at start-up time, but in principle it's possible to restart the server once you've fixed the configuration.

While this approach works for most web apps, it does not fully restart Python, so I suspect the right approach is to use a separate process to run a "management server" that has the ability to restart the main server and/or put it into configuration mode.  This would also let us set up other things on the Pi, for example to activate/deactivate a Jupyter server or to configure voice recognition.

RWB had been considering building this in to the existing `openflexure_microscope` module.  However, that would be quite messy and would introduce some stronger dependencies on the system configuration of the microscope.

Instead, RWB will make the management server a completely separate application.  That way, it's tied to the *system* and is specific to the Raspberry Pi image rather than being bundled with the microscope server.  This feels like a very sensible move:

* It greatly reduces the need for users to make an SSH connection with the microscope, which reduces the risk of them breaking it or doing unexpected things to the software.
* It gives us more control over how the microscope is configured - e.g. the medical version could restrict the use of plugins to a whitelist.
* It reduces the likelihood of the configuration file being invalid because it's been edited by hand.
* It make the user experience much better for less technical users, who are uncomfortable with SSH and the terminal.

We've not yet agreed a timeline for implementing such a management server, but we agreed to record this discussion for future reference.


**Meeting ends: TIME**

