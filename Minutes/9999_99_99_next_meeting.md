## 08 May 2024

**Meeting starts: 14:15**

Attendees
* Richard Bowman (Glasgow)
* Joe Knapper (Glasgow)
* Julian Stirling (Freelance)
* William Wadsworth (Bath)
* Filip Ayazi (Cambridge)

## Agenda

1. Apologies
2. Minutes of last meeting
3. Matters arising
4. Review of OFEP MRs
5. Slide holder design/requirements

## Actions



## Apologies

None

## Minutes of the last meeting

Approved.

## Matters arising

* **done**: RWB to make an [issue #2](https://gitlab.com/openflexure/microscope-failure-mode-analysis/-/issues/2) on the failure mode analysis repo suggesting we do a trawl of issues.
* **action**: WJW to check if !343 has spotted a bug in the optics module - he believes there may be a problem but hasn't yet raised an issue
* **action**: RWB to publish packages for server dependencies.
* **action**: RWB to add slide holder design to a future agenda (this one?).
