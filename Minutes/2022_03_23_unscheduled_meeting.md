## 23 March 2022

These minutes are from the regular call between Bongo Tech & Research Labs (BTech) and the team at the University of Bath (UoB).  This call has been adopted into the design review repository as it was relevant to a number of ongoing issues.

**Meeting starts 09:45 GMT/12:45 EAT**

Attendees:
* Paul Nyakyi - PN (BTech)
* Richard Bowman - RWB(UoB)
* Julian Stirling - JS (UoB)
Other people mentioned:
* Joram Mduda - JM (Ifakara Health Institute)
* Joel Collins (Former PDRA, UoB)

## Agenda
1. Report on microscope commissioning
2. Update from BTech on premises

# 1. Report on microscope commissioning

There is an ongoing order for 10 microscopes with 100x objectives, version `v7.0.0-alpha1`, to be delivered by BTech to Ifakara Health Institute (IHI), one of the final deliverables from the EPSRC malaria diagnostics project.  The first batch of 4 microscopes was delivered at the end of January, and RWB has been working with Joram Mduda, technician at IHI, to set them up and verify their performance.  A number of relevant issues have arisen.

## 1.1. Software and security

Microscope performance was measured using a set of ad-hoc scripts, run by RWB over a remote connection.  We agreed it would be very useful to incorporate these scripts into the microscope software so they can be run conveniently and consistently during pre-release testing and/or installation.

It would be particularly advantageous to record the history of calibrations - currently, recalibrating the microscope overwrites previous calibration data.  Retaining this data would be helpful for debugging.  It would also be very useful to retain the manufacturer's calibration data.  This would allow the microscope to be reset to factory settings, which is a very useful mitigation in the event that it is recalibrated incorrectly. Recording factory settings would also allow us to ascertain that the microscope's performance has not degraded since it was initially calibrated by the manufacturer.

An important issue we raised is that doing this reliably requires some level of permissions management.  Currently, the microscope allows the default user, `pi`, to change any settings through the use of an unrestricted `sudo` privilege.  An IVD product must implement at least a separation between "user" and "manufacturer" privileges in order to have features like factory reset (described above) work reliably.  It is probably also desirable to introduce a third permission level, intended for use by a competent technician in the customer's organisation.  This intermediate level would mean that only trained technicians can recalibrate the microscope, for example, while regular users are restricted to routine operations that don't affect the microscope's settings.

Implementing this would require some changes to the pre-installed operating system, restricting the default user's ability to modify the system with `sudo`, while preserving the ability to use privilege escalation for some commands such as starting or stopping the microscope software.  Authentication for the different functions in the microscope software would need to be handled by the server, rather than simply providing modified client software - the latter would not be secure.

RWB noted that there is currently no authentication built in to the server, but that it was considered when the server architecture was designed, in discussions between him and Joel Collins.  Libraries exist that would add minimal code into the server to restrict certain operations to certain priviledge levels.  Authentication of individual users, including password management, can then be devolved to another web service.  This might be run locally on the microscope (for a stand-alone system), or could integrate with an authentication service elsewhere, e.g. provided by a hospital or healthcare system.  Clearly the latter would require a network connection.

We need to do a permissions audit before releasing a medical product:
* The server must run as a less privileged user, so it can't change read-only files like the factory settings.  It already runs as `openflexure-ws` rather than `root`, so it's simply a matter of reviewing the permissions for that user.
* The `pi` user should not have global `sudo` privileges.  We will need to review and enable `sudo` for specific tasks.  We may want to use a different name for the default user.
* BTech (i.e. the manufacturer) will need to hold an administrative password for an appropriate user account, we should probably create this account on the SD card image.
* It may be desirable to allow some operations without authentication, particularly those for regular users.

## 1.2. Vibration

Joram experienced issues due to vibration affecting the image quality, which we mitigated temporarily by using a heavy book on some packaging foam.  We previously discussed with BTech sourcing suitably sized (200-300mm square) pieces of synthetic marble.  It seems these should be readily available at a reasonable price, as they can be sourced form offcuts from kitchen worktops.  Including a marble slab in the microscope product would only add a few percent to the cost, and significantly improve performance for high-resolution versions.  BTech have been busy with their new premises and have not yet managed to source any blocks for testing.

## 1.3. Issues noted during commissioning

### 1.3.1. Phillips screws

Some of the screws in Joram's photographs were Phillips head, not socket head as specified.  PN explained that this was a deliberate substitution because BTech had not been able to source M3 socket head cap screws of the correct length in the time available.  He confirmed that they are M3 brass screws and that there are nuts in the nut traps, rather than self tapping screws.

### 1.3.2. Lens shading calibration

PN burned fresh SD cards from the image available on [openflexure.org](https://openflexure.org/), and calibrated each microscope individually.  This means that all the cameras were working correctly and free from dirt when they were set up at BTech.

### 1.3.3. XY calibration

PN used a monitor connected directly to the microscope for calibration, and so didn't see the calibration wizard dialog on first run.  This means he did not attempt XY calibration as he was not prompted to.  

**Action** Note this as a failure mode, and suggest a mitigation, e.g. including XY calibration in a checklist before products are released.

### 1.3.4. Optics module dirt

One of the microscopes had dirt on the camera sensor.  This must have fallen from the optics module after it was calibrated.  

**Action** We should add a cleaning step (e.g. using compressed air) to ensure the optics module is free from dust before it is assembled.

### 1.3.5. Ribbon cable

One microscope did not have a good connection to the camera.  JM replaced the ribbon cable and resolved the issue.  RWB did not note whether it was a new ribbon cable or the same one (i.e. whether a component was damaged, or just badly connected).  

**Action** RWB will follow up with JM.

## 2. Update from BTech on premises

BTech are building a new "G+2" building (3 floors) largely as a result of having received a capacity-building grant and needing more space.  The plan is to have social space and offices at the top, offices/classes on first floor, a ground floor workshop, and a separate workshop in an adjacent one-storey building.  They have a grant to fund £100k of machines from [BOTNAR].  They've already started work on the main building - construction of the separate workshop starts on Monday.  The ground floor of the larger building should be done within 2 months (so they can move from existing premises).  BTech are looking for funding for the first floor, the second floor ("+2") is a future goal.  Current budget for ground floor is 15k USD, which is very tight - this means the BTech team are putting a lot of effort into sourcing high quality materials at the best possible price.  BTech will contact TMDA to transfer registration when G+1 is complete and microscope-related activities have moved to the new premises.

[BOTNAR]: https://www.fondationbotnar.org/

As part of this project, BTech needs to buy some SLA/SLS printers.  One application for them is a project modelling teeth.  RWB mentioned that Fergus Riche (PhD student at University of Cambridge) used a service to have some early microscopes printed using SLS (Selective Laser Sintering) Nylon in 2016.  They seemed incredibly robust; the nylon was very tough, and interlayer adhesion was excellent.  We didn't test quantitatively, but the parts were much tougher than PLA.  However, both machine costs and running costs were an order of magnitude higher than FFF, so it wasn't worth exploring for microscope production at the time.  If and when BTech have an SLS machine available, it would be interesting to use it to make some microscopes, and evaluate their performance.

**Meeting ends 10:22 GMT/13:22 EAT**