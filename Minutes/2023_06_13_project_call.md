## 16 June 2023

*This was a call of the current active developers, but it has been adopted into the Design Review repository as its content is very relevant here. Actions such as confirming minutes should wait for a future Design Review meeting that's formally scheduled as such.*

**Meeting starts: 1405**

Attendees
* Richard Bowman
* Joe Knapper
* Freya Whiteford
* Filip Ayazi
* William Wadsworth

## Update since last meeting

We've released a beta of the microscope, and various other fixes since mean we may now be ready for another beta.

Richard has made progress on the software, largely by starting a [ground-up reimplementation of LabThings](https://github.com/rwb27/labthings-fastapi), which has now reached the stage where it's worth trying to implement a few key Things (like stage and camera). Principles he's adopted so far include:

* Using FastAPI to keep server-side code clean and simple
* Using Pydantic and Python type annotations, as far as possible avoiding the need for separate schema definitions
* Co-developing a Python client, such that code is more easily portable between server-side Things and client-side Python

The bottleneck here will be Richard's time: the roadmap has us releasing the new version of software coincident with v7, but this may now be delayed until July at the earliest.

Updating to LabThings-fastapi will break existing server/Extension code, but hopefully the required changes are not huge.

## Releasing v7 of the hardware

Currently there are several configurations of hardware in the repo: the inverted version is well tested, but others are less well tested. It would be nice to split things - but for now we probably just need to be clear about what's "ready" and what's less well tested.

The one feature to consider before the next beta is reinstating low res webcam models (!309), which does affect the low res optics: this would need code review. It definitely shouldn't go in between beta and v7, but whether it's v7 or v7.1 can remain a question of when we can do code review and how urgently we want a beta.

Modified bases for different electronics will get merged very soon.

Beta 2 might be useful now: there are a few things that are wrong with `beta1` that are now fixed, e.g. nano converter plate. Let's release `beta2` as soon as the electronics drawer is merged, and plan a `beta3` to include some instructions fixes and possibly the webcam changes. After that, hopefully we can release v7 with minimal tweaks.

## Matters arising from previous meetings

**Action:** Note obscuration of the first-run wizard as a failure mode: still outstanding

**Action:** Add a cleaning step to the instructions, before fitting the camera.  Still outstanding.

**Action**: write an OFEP describing how we define the "interface", with some principles about which bits count and which don't.  An actual definition of the microscope interface probably belongs in the microscope documentation. Still outstanding

**Action**: create an issue to remind us to put some sort of extensions gallery together before releasing v7. Still outstanding.

## Software v3

* Shift to FastAPI as discussed above
* Configuration as previously discussed
* Updating to new picamera library - Filip has [implemented some amount of support for Bullseye already using picamera2](https://openflexure.discourse.group/t/new-autofocus-camera-modules-raspberry-pi-camera-module-3/1125/13).
* Forcing Things to interact using their public API, unless explicit exceptions are made to that rule (e.g. to calibrate a picamera). This should really help code maintenance in the future, as well as making development easier by making client and server code use the same syntax.
* Including lots more characterisation routines e.g. auto-centring, resolution testing, etc.

Next steps: Richard will try to implement a basic Thing for the sangaboard, and also the camera - Filip's current code is linked to from the forum somewhere.

We need some sort of management server to manage Things and configure the server.

## Sangaboard v5

Many thanks and congratulations to Filip for making v5 happen - it is so much neater and more reliable than the previous versions.

We will make the instructions consistent that we use Sangaboard v0.5 (v0.5.2). Filip has about 50 of these in stock, and about 8 of v0.5.1. He's sold about 60 in total. v0.5.2 is more or less identical, except a series resistor change for on-board LED brightness, cuttable traces for onboard LEDs, and a fix to the USB-C wiring.

The documentation is still not really written. Filip is planning to ship them with the latest firmware pre-installed.

Documentation is needed for the illumination options, it's important to be clear how they are wired up.  Also need to document how to make extension boards. Interactive control of the illumination is currently done via an extension.  Filip's LED board will, by default, light up at 30% intensity (30mA) without additional software.

Conclusion: keep the 5v LED as the default board, with a link to the LED workaround, and a note that Filip's constant current driven board uses different pins on the Sangaboard.

Where do we put instructions about how to get firmware and flash it onto the board? Currently this is done in CI. 

Conclusion: we should probably put the Sangaboard firmware on the openflexure build server.

Filip's extension already has built-in flashing capabilities for the Sangaboard v5. For the Nano workaround, we could install `avrdude` and the firmware, then a single command will upload it to the connected Arduino.

Conclusion: get `avrdude` in the SD image, with a simple command to flash it. A terminal or SSH connection is probably the right interface for this, though we could build it into the management server.

**Meeting stops: 1503**
