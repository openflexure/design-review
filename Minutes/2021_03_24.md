## 24 March 2021

**Meeting starts: 14:31**

Attendees
* Richard Bowman
* William Wadsworth
* Julian Stirling


## Agenda

1. Apologies
1. Minutes of the last meeting (10/3/21)
1. Matters arising from last meeting
1. Discussion of the git-flow and release philosophy in general.
1. Which software repos need protecting?
1. Should we protect release tags
1. What git-flow are we now using on the software repository? There is a stable and a master branch protected. Is this still the flow?
1. How do we start adding quality control to the software repository
1. AOB



## 1. Apologies
None

## 2. Minutes of the last meeting (10/3/21)
Minutes will be confirmed by email

## 3. Matters arising from last meeting

All issues that needed to be opened were. And the 3 discussed merge requests were merged.

## 4. Discussion of the git-flow and release philosophy in general.

* We should have a road map of features that are going to be in the next releases. **Action**: find somewhere to write this out nicely
* We need to have alpha releases so that people can play with the new version and see their code going somewhere
* **Action**: Write an OFEP about how we split parts of the project, and do dependency management.
* Consider doing a 6.2 release with the new fluorescence stuff.
* Finish code cleanup is higher priority than any other features right now.
* Before clean up we need to merge in a load of outstanding stuff.

## 5. Which software repos need protecting?
* Server
* Pi image.
* Command line tools

**Action** Check these are protected and have merge rules

* camera-stage-mapping - will be controlled by Richard and is a python dependency.

## 6. Should we protect release tags

YES! for all repos that we have approval rules for.
**Action** investigate protected tags

## 7. What git-flow are we now using on the software repository?
JS asks: There is a stable and a master branch protected. Is this still the flow?
RWB: No.  Stable branch is stale and should be archived (tagged and deleted?).  We could do with a stale branch pruning session, and a merge-fest once deps are fixed to close outstanding MRs.

``master`` is the development branch.  Releases are tagged, and patch releases can be done on branches from the tagged releases.  We have not yet encountered a situation where we have more than one feature release at any one time.  As a rule, everything merges onto master, and anything I'm likely to make a big mess with I've been doing in a separate repo.  I think this is roughly in line with how we treat the microscope, possibly with more of a tendency to try to keep master releasable.

**Action** work out the best way to stop stable being a branch without loosing commits.

## 8. How do we start adding quality control to the software repository

* A release roadmap/plan would be a good start.
* Approvals are helping with new code.
* Kaspar and Richard are doing a lot of going through the code to build understanding now Joel is gone.
* A code audit is probably needed at some point. This is not now.

## 9. AOB
* Condenser lens should probably be flat side towards focus. Probably doesn't matter, need to check STL **Action** make issue.
* Need to clean up the number of branches on microscope repo **Action** make issue to do this

**Meeting ends: 15:45**
