# Issues

## Hardware
[#124](https://gitlab.com/openflexure/openflexure-microscope/-/issues/124) - O-rings snap. Needs a new locking tool?. Needs instruction images to have band tool cover in use. Also [Forum post](https://openflexure.discourse.group/t/extending-openflexure-to-schools/1288/7) and [#304](https://gitlab.com/openflexure/openflexure-microscope/-/issues/304) below.  
[#192](https://gitlab.com/openflexure/openflexure-microscope/-/issues/192) - Viton band specification and making the column top-out before crushing O-ring.

[#157](https://gitlab.com/openflexure/openflexure-microscope/-/issues/157) - Pi 2.1, also a forum post about software, maybe related to Pi 2.0. Deprecate Pi 2.1? or test Pi 2.1?

[#158](https://gitlab.com/openflexure/openflexure-microscope/-/issues/158) - focus of infinity optics module. Needs chacking and probably needs correcting still. Also needs instructions for checking the focus. Can we also check focus for teh standard 160?

[#164](https://gitlab.com/openflexure/openflexure-microscope/-/issues/164) - SCAD clean up. Do it or close it?

[#170](https://gitlab.com/openflexure/openflexure-microscope/-/issues/170) - not directly this issue, but there is nothing on reflection or fluorescence in the instructions. Has it even been tested recently on the Microscope? Does the cut-out actually fit? see [#243](https://gitlab.com/openflexure/openflexure-microscope/-/issues/243) below.

A group of tidying up the code, which mostly can stay as issues but don't need to be done for v7:
[#175](https://gitlab.com/openflexure/openflexure-microscope/-/issues/175) - make `tiny()` an exact binary?
[#178](https://gitlab.com/openflexure/openflexure-microscope/-/issues/178) - uniform clearance hole sizes.
[#314](https://gitlab.com/openflexure/openflexure-microscope/-/issues/314) - uniform nut sizes.
[#318](https://gitlab.com/openflexure/openflexure-microscope/-/issues/318) - uniform nut sizes, rationalise nut modules.
[#290](https://gitlab.com/openflexure/openflexure-microscope/-/issues/290) - vertical nut traps
[#300](https://gitlab.com/openflexure/openflexure-microscope/-/issues/300) - hard coded distance to replace with parameters
[#308](https://gitlab.com/openflexure/openflexure-microscope/-/issues/308) - hole from bottom
[#309](https://gitlab.com/openflexure/openflexure-microscope/-/issues/309) - nut in optics module solved by !334
[#310](https://gitlab.com/openflexure/openflexure-microscope/-/issues/310) - solved in !338
[#311](https://gitlab.com/openflexure/openflexure-microscope/-/issues/311) - Solved in !338


[#198](https://gitlab.com/openflexure/openflexure-microscope/-/issues/198) - Make screw pockets deep enough for 10mm screws to trap the nuts. Illumination mount and probably stand as well. Check wehere needed, or make a tool to make an m3x10 into a nut seater tool.
[#294](https://gitlab.com/openflexure/openflexure-microscope/-/issues/294) - same issue in stand

[#202](https://gitlab.com/openflexure/openflexure-microscope/-/issues/202) - Change size of cable tidies on x and y?

[#206](https://gitlab.com/openflexure/openflexure-microscope/-/issues/206) - brim and smart brim - what do we do, or what do we advise? Note also that the newer Prusa Slicer is making the smart brim more separate from the body - the default elephants foot reduction seems to be more agressive than before.
also [#264](https://gitlab.com/openflexure/openflexure-microscope/-/issues/264) - smart brim definately on `leg_test.stl`

[#207](https://gitlab.com/openflexure/openflexure-microscope/-/issues/207) - version string on renders. I think this works? or maybe it is not taking the tag name - the Beta does not say Beta it gives the hash.
[#283](https://gitlab.com/openflexure/openflexure-microscope/-/issues/283) - version numbering parts. Related is type numbering parts - infinity and 160mm optics and bases.

[#213](https://gitlab.com/openflexure/openflexure-microscope/-/issues/213) - (which?) dovetail may not work if not used as we use it. This is marked blocking, is it?


Instructions cleanup issue group
[#215](https://gitlab.com/openflexure/openflexure-microscope/-/issues/215) - style (discuss)
[#225](https://gitlab.com/openflexure/openflexure-microscope/-/issues/225)
[#230](https://gitlab.com/openflexure/openflexure-microscope/-/issues/230)
[#243](https://gitlab.com/openflexure/openflexure-microscope/-/issues/243) - reflection/ fluorescence, also noted above.
[#254](https://gitlab.com/openflexure/openflexure-microscope/-/issues/254)
[#265](https://gitlab.com/openflexure/openflexure-microscope/-/issues/265)
[#266](https://gitlab.com/openflexure/openflexure-microscope/-/issues/266)
[#272](https://gitlab.com/openflexure/openflexure-microscope/-/issues/272) - style (discuss)
[#273](https://gitlab.com/openflexure/openflexure-microscope/-/issues/273) - GLB is already done?
[#278](https://gitlab.com/openflexure/openflexure-microscope/-/issues/278) 
[#291](https://gitlab.com/openflexure/openflexure-microscope/-/issues/291) - print settings for optics modules?
[#295](https://gitlab.com/openflexure/openflexure-microscope/-/issues/295) - electronics workaround
[#296](https://gitlab.com/openflexure/openflexure-microscope/-/issues/296) - illumination work-around
[#301](https://gitlab.com/openflexure/openflexure-microscope/-/issues/301) - probably needs a new Gitbuilding instruction, suggested in [Gitbuilding #259](https://gitlab.com/gitbuilding/gitbuilding/-/issues/259)
[#302](https://gitlab.com/openflexure/openflexure-microscope/-/issues/302) - motor screws
[#303](https://gitlab.com/openflexure/openflexure-microscope/-/issues/303) - part numbers
[#304](https://gitlab.com/openflexure/openflexure-microscope/-/issues/304) - related to [#124] above
[#307](https://gitlab.com/openflexure/openflexure-microscope/-/issues/307) - electronics
[#305](https://gitlab.com/openflexure/openflexure-microscope/-/issues/305) - electronics - this is sorted I think on Sangaboard firmware. 
[#312](https://gitlab.com/openflexure/openflexure-microscope/-/issues/312) - upright instructions
[#315](https://gitlab.com/openflexure/openflexure-microscope/-/issues/315) - electronics workaround
[#316](https://gitlab.com/openflexure/openflexure-microscope/-/issues/316) - knowledge base
[#317](https://gitlab.com/openflexure/openflexure-microscope/-/issues/317) - infinity optics


[#244](https://gitlab.com/openflexure/openflexure-microscope/-/issues/244) - sangaboard 3 drawer/base. Completed in !319

[#257](https://gitlab.com/openflexure/openflexure-microscope/-/issues/257) - illumination aperture, use it or lose it (duplicated in [#313])
[#313](https://gitlab.com/openflexure/openflexure-microscope/-/issues/313) - basically a duplicate of [#257]

[#260](https://gitlab.com/openflexure/openflexure-microscope/-/issues/260) - stabilisation of optics dovetail during print. Similarly stabilisation of bridges in base during print?

[#271](https://gitlab.com/openflexure/openflexure-microscope/-/issues/271) - changelog

[#274](https://gitlab.com/openflexure/openflexure-microscope/-/issues/274) - CI keeps `.d` files in final docs

[#277](https://gitlab.com/openflexure/openflexure-microscope/-/issues/277) - the image in the main `readme` is missing, but would be the v6 render. Also the image in the main instructions is a v6 render which needs to be replaced with v7, probably a photograph?

[#284](https://gitlab.com/openflexure/openflexure-microscope/-/issues/284) - saving build hashes

[#289](https://gitlab.com/openflexure/openflexure-microscope/-/issues/289) - camera platform heights, fixed by !309 webcam parts

[#292](https://gitlab.com/openflexure/openflexure-microscope/-/issues/292) - space above nuts in electronics drawer



## Web site
microscope issue [#73](https://gitlab.com/openflexure/openflexure-microscope/-/issues/73) - out of date instructions elsewhere - this appears to be complete?



## Server
If there is provision for other cameras, then in the hardware:
[#306](https://gitlab.com/openflexure/openflexure-microscope/-/issues/306) - Pi camera 3 and [#146](https://gitlab.com/openflexure/openflexure-microscope/-/issues/146) High-Q
