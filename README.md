# OpenFlexure Microscope Design Review

The OpenFlexure Microscope design has been has been developed via a combination of academic and online collaboration. The microscope has been made and used by researchers and hobbyist, and has been undergoing continuous improvement via incorporating feedback and open source design development. As the project moves towards a product that can be be sold, rather than a prototype design for research purposes we are beginning to formalise some of our design process. This does not replace the discussions on issue threads, merge requests, and in the forum, it instead is designed to ensure that any changes to the design resulting from these discussions is properly documented, and will identify further changes that are necessary.

The formalisation includes:
* **[OpenFlexure Enhancement Proposals (OFEPs)](https://ofep.openflexure.org/)** - Documents which propose a major design change, or document major design decisions. If accepted these documents serve as in depth documentation for the project.
* **[Failure Mode Analysis](https://gitlab.com/openflexure/microscope-failure-mode-analysis)** - This will be a systematic review of the failure modes of the microscope. This is separated into failures that affect manufacturing (i.e. process failures that delays production) and failures that affect end users (i.e. microscope failure during use, even if the root cause was a manufacturing problem).
* **Design Review Meetings** - Minuted meetings to discus the design. These meetings will cover top-down overview discussions of the design. They will also be used to cover discussions of feedback on the microscope, progress of implementing features, and discussions of proposed changes. Some in depth topics will be referred to the OFEP process.

This repository contains the minutes for the design review meetings, and other associated documentation.

## Documents

* [Intended Use and Requirements Draft](Docs/intended_use_and_requirements.md)

## Minutes

* [8th February 2021](Minutes/2021_02_08.md)
* [24th February 2021](Minutes/2021_02_24.md)
* [10th March 2021](Minutes/2021_03_10.md)
* [24th March 2021](Minutes/2021_03_24.md)
* [7th April 2021](Minutes/2021_04_07.md)
* [28th April 2021](Minutes/2021_04_28.md)
* [19th May 2021](Minutes/2021_05_19.md)
* [23rd June 2021](Minutes/2021_06_23.md)
* [14th July 2021](Minutes/2021_07_14.md)
* [18th August 2021](Minutes/2021_08_18.md)
* [17th March 2022](Minutes/2022_03_17.md)
* [23rd March 2022](Minutes/2022_03_23_unscheduled_meeting.md)
* [13th April 2022](Minutes/2022_04_13_unscheduled_meeting.md)
* [22nd September 2022](Minutes/2022_09_22.md)
* [3rd November](Minutes/2022_11_03.md)
* [13th June 2023](Minutes/2023_06_13_project_call.md)
* [5th October 2023 - Issues paper](Minutes/2023_10_05_meeting_paper-Issues_to_7_0_0.md)
